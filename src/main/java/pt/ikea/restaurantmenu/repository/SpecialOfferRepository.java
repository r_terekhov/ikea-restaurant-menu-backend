package pt.ikea.restaurantmenu.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pt.ikea.restaurantmenu.domain.SpecialOffer;

@Repository
public interface SpecialOfferRepository extends MongoRepository<SpecialOffer, String> {

}
