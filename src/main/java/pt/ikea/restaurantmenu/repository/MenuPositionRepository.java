package pt.ikea.restaurantmenu.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pt.ikea.restaurantmenu.domain.MenuPosition;

import java.util.List;

@Repository
public interface MenuPositionRepository extends MongoRepository<MenuPosition, String> {

    List<MenuPosition> findAllByVisibleInCatalogueTrueOrderByCategoryOrderAsc();
}
