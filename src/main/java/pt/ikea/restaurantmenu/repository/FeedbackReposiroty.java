package pt.ikea.restaurantmenu.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pt.ikea.restaurantmenu.domain.Feedback;

@Repository
public interface FeedbackReposiroty extends MongoRepository<Feedback, String> {
}
