package pt.ikea.restaurantmenu.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pt.ikea.restaurantmenu.domain.OnlyPartOfPosition;

@Repository
public interface OnlyPartOfPositionRepository extends MongoRepository<OnlyPartOfPosition, String> {

}
