package pt.ikea.restaurantmenu.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pt.ikea.restaurantmenu.domain.Category;

import java.util.List;

@Repository
public interface CategoryRepository extends MongoRepository<Category, String> {

    List<Category> findAllByOrderByOrderAsc();
}
