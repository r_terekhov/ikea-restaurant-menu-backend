package pt.ikea.restaurantmenu.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import pt.ikea.restaurantmenu.service.InitService;

@Component
public class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {
    private final InitService initService;


    @Autowired
    public ApplicationStartup(InitService initService) {
        this.initService = initService;
    }

    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event) {


        //initService.initDataBase();

    }

}
