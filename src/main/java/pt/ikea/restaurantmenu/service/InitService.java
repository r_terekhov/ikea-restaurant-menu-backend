package pt.ikea.restaurantmenu.service;

public interface InitService {

    void initDataBase();
}
