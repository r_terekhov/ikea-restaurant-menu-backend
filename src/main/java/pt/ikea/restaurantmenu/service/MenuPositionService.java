package pt.ikea.restaurantmenu.service;

import pt.ikea.restaurantmenu.domain.Position;
import pt.ikea.restaurantmenu.domain.SpecialOffer;

import java.util.List;

public interface MenuPositionService {

    List<Position> getAllPositions();

    List<SpecialOffer> getAllSpecialOffers();
}
