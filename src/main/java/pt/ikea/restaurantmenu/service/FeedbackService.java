package pt.ikea.restaurantmenu.service;

import javax.validation.constraints.NotBlank;

public interface FeedbackService {

    String postFeedback(@NotBlank String feedback);
}
