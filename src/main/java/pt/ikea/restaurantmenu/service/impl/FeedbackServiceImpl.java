package pt.ikea.restaurantmenu.service.impl;

import org.springframework.stereotype.Service;
import pt.ikea.restaurantmenu.domain.Feedback;
import pt.ikea.restaurantmenu.repository.FeedbackReposiroty;
import pt.ikea.restaurantmenu.service.FeedbackService;

import javax.validation.constraints.NotBlank;

@Service
public class FeedbackServiceImpl implements FeedbackService {
    private final FeedbackReposiroty feedbackReposiroty;

    public FeedbackServiceImpl(FeedbackReposiroty feedbackReposiroty) {
        this.feedbackReposiroty = feedbackReposiroty;
    }

    @Override
    public String postFeedback(@NotBlank String feedback) {
        return feedbackReposiroty.save(new Feedback(feedback)).getId();
    }
}
