package pt.ikea.restaurantmenu.service.impl;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import pt.ikea.restaurantmenu.domain.Category;
import pt.ikea.restaurantmenu.domain.MenuPosition;
import pt.ikea.restaurantmenu.domain.OnlyPartOfPosition;
import pt.ikea.restaurantmenu.domain.SpecialOffer;
import pt.ikea.restaurantmenu.dto.ExtendsOf;
import pt.ikea.restaurantmenu.model.BGU;
import pt.ikea.restaurantmenu.repository.CategoryRepository;
import pt.ikea.restaurantmenu.repository.MenuPositionRepository;
import pt.ikea.restaurantmenu.repository.OnlyPartOfPositionRepository;
import pt.ikea.restaurantmenu.repository.SpecialOfferRepository;
import pt.ikea.restaurantmenu.service.InitService;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

@Profile("local")
@Service
public class InitServiceLocal implements InitService {

    private final CategoryRepository categoryRepository;
    private final MenuPositionRepository menuPositionRepository;
    private final SpecialOfferRepository specialOfferRepository;
    private final OnlyPartOfPositionRepository onlyPartOfPositionRepository;

    public InitServiceLocal(CategoryRepository categoryRepository, MenuPositionRepository menuPositionRepository, SpecialOfferRepository specialOfferRepository, OnlyPartOfPositionRepository onlyPartOfPositionRepository) {
        this.categoryRepository = categoryRepository;
        this.menuPositionRepository = menuPositionRepository;
        this.specialOfferRepository = specialOfferRepository;
        this.onlyPartOfPositionRepository = onlyPartOfPositionRepository;
    }


    @Override
    public void initDataBase() {

        Category category1 = new Category("Салаты/холодные закуски", "это вкусно", "url", 0);
        Category savedCategory1 = categoryRepository.save(category1);

        Category category2 = new Category("Десерты", "это вкусно", "url", 1);
        Category savedCategory2 = categoryRepository.save(category2);

        Category category3 = new Category("Гарниры", "это вкусно", "url", 2);
        Category savedCategory3 = categoryRepository.save(category3);

        MenuPosition menuPosition1 = new MenuPosition(
                "MENU_POSITION",
                "Маринованный лосось",
                "самый вкусный лосось",
                "https://firebasestorage.googleapis.com/v0/b/ikea-20c86.appspot.com/o/restaurant-menu%2ForiginalPhotos%2F%D0%9C%D0%B0%D1%80%D0%B8%D0%BD%D0%BE%D0%B2%D0%B0%D0%BD%D0%BD%D1%8B%D0%B9%20%D0%BB%D0%BE%D1%81%D0%BE%D1%81%D1%8C.jpg?alt=media&token=bd1e5cde-14d4-4b65-8472-7000ddbbc190",
                "Лосось (Атлантический лосось (salmo salar), соль, сахар, белый перец, укроп), салатная смесь Тоскана (салаты раддичо, романо и фриссе), соус для лосося (уксус, горчица, укроп, сахар, растительное масло), лимон, укроп.",
                new BGU(14.3, 15.3, 5.6, 218),
                "горчица,злаки, рыба и продукты  их переработки.",
                "106",
                199,
                159,
                savedCategory1.getName(),
                savedCategory1.getId(),
                savedCategory1.getOrder(),
                "930348966", null, true);
        MenuPosition savedMenuPosition1 = menuPositionRepository.save(menuPosition1);


        MenuPosition menuPosition2 = new MenuPosition(
                "MENU_POSITION",
                "Картофель фри",
                null,
                "https://firebasestorage.googleapis.com/v0/b/ikea-20c86.appspot.com/o/restaurant-menu%2FcustomPhotos%2F%D0%BA%D0%B0%D1%80%D1%82%D0%BE%D1%84%D0%B5%D0%BB%D1%8C%20%D1%84%D1%80%D0%B8.jpg?alt=media&token=69d11a25-4b60-4b07-b03a-8c2bbc7b62bf",
                "Картофель, масло растительное",
                new BGU(4.9, 16.8, 43.4, 345),
                null,
                "150",
                69,
                44,
                savedCategory3.getName(),
                savedCategory3.getId(),
                savedCategory3.getOrder(),
                "450374978",
                new LinkedList<>(Collections.singleton(new ExtendsOf("Гарнир", List.of("123")))), true);
        MenuPosition saveMenuPosition2 = menuPositionRepository.save(menuPosition2);

        SpecialOffer specialOffer = new SpecialOffer("Завтрак за 89",
                "описание",
                "url",
                123,
                "category",
                new LinkedList<>(Collections.singleton(new ExtendsOf("Гарнир", List.of("123")))));


        specialOfferRepository.save(specialOffer);

        OnlyPartOfPosition onlyPartOfPosition = new OnlyPartOfPosition("ONLY_PART_OF_POSITION", "name", "des", "url");
        onlyPartOfPositionRepository.save(onlyPartOfPosition);
    }
}
