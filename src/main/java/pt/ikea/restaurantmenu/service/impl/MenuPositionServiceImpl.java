package pt.ikea.restaurantmenu.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pt.ikea.restaurantmenu.domain.Position;
import pt.ikea.restaurantmenu.domain.SpecialOffer;
import pt.ikea.restaurantmenu.repository.MenuPositionRepository;
import pt.ikea.restaurantmenu.repository.OnlyPartOfPositionRepository;
import pt.ikea.restaurantmenu.repository.SpecialOfferRepository;
import pt.ikea.restaurantmenu.service.MenuPositionService;

import java.util.ArrayList;
import java.util.List;

@Service
public class MenuPositionServiceImpl implements MenuPositionService {
    private static final Logger log = LoggerFactory.getLogger(MenuPositionService.class);
    private final MenuPositionRepository menuPositionRepository;
    private final SpecialOfferRepository specialOfferRepository;
    private final OnlyPartOfPositionRepository onlyPartOfPositionRepository;

    public MenuPositionServiceImpl(MenuPositionRepository menuPositionRepository, SpecialOfferRepository specialOfferRepository, OnlyPartOfPositionRepository onlyPartOfPositionRepository) {
        this.menuPositionRepository = menuPositionRepository;
        this.specialOfferRepository = specialOfferRepository;
        this.onlyPartOfPositionRepository = onlyPartOfPositionRepository;
    }


    @Override
    public List<Position> getAllPositions() {
        List<Position> positions = new ArrayList<>(menuPositionRepository.findAllByVisibleInCatalogueTrueOrderByCategoryOrderAsc());
        positions.addAll(onlyPartOfPositionRepository.findAll());
        return positions;
    }

    @Override
    public List<SpecialOffer> getAllSpecialOffers() {
        return specialOfferRepository.findAll();
    }
}
