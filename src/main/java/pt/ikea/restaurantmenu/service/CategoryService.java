package pt.ikea.restaurantmenu.service;

import pt.ikea.restaurantmenu.domain.Category;

import java.util.List;

public interface CategoryService {

    List<Category> getAllCategories();

}
