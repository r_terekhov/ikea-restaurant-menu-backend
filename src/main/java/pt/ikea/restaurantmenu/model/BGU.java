package pt.ikea.restaurantmenu.model;

import lombok.Value;

@Value
public class BGU {

    double proteins;

    double fats;

    double carbohydrates;

    int calories;
}
