package pt.ikea.restaurantmenu.dto;

import lombok.RequiredArgsConstructor;
import lombok.Value;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Value
@RequiredArgsConstructor
public class ExtendsOf {

    @NotBlank
    String category;

    @NotNull List<String> positions;
}
