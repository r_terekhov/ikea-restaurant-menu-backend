package pt.ikea.restaurantmenu.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotBlank;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
public class OnlyPartOfPosition extends Document implements Position {
    private String type = "ONLY_PART_OF_POSITION";

    @NotBlank String name;

    @Nullable
    String description;

    @NotBlank String urlPhoto;

    @Override
    public String getPositionType() {
        return this.type;
    }
}
