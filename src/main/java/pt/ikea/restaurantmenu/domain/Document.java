package pt.ikea.restaurantmenu.domain;

import lombok.Getter;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.Instant;

@org.springframework.data.mongodb.core.mapping.Document
public abstract class Document {
    @Id
    @Getter
    @NotBlank
    private String id;


    @Getter
    @NotNull
    private long created;


    public Document() {
        this.created = Instant.now().toEpochMilli();
    }
}
