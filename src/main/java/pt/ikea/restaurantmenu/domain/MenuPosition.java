package pt.ikea.restaurantmenu.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.lang.Nullable;
import pt.ikea.restaurantmenu.dto.ExtendsOf;
import pt.ikea.restaurantmenu.model.BGU;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.LinkedList;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MenuPosition extends Document implements Position {
    private String type = "MENU_POSITION";

    @NotBlank String name;

    @NotBlank String description;

    @NotBlank String urlPhoto;

    @NotBlank String consist;

    @NotNull BGU bgu;

    @Nullable
    String allergens;

    @NotBlank String portionSize;

    int cost;

    @Nullable
    Integer ikeaFamilyCost;

    @NotBlank String category;

    @NotBlank String categoryId;

    @NotBlank Integer categoryOrder;

    @NotBlank String vendorCode;

    @NotNull LinkedList<ExtendsOf> extendsOf;

    boolean visibleInCatalogue;


    @Override
    public String getPositionType() {
        return this.type;
    }
}
