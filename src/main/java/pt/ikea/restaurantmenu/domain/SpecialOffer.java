package pt.ikea.restaurantmenu.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import pt.ikea.restaurantmenu.dto.ExtendsOf;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.LinkedList;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SpecialOffer extends Document {
    @NotBlank String name;

    @NotBlank String description;

    @NotBlank String urlPhoto;

    int cost;

    @NotBlank String category;

    @NotNull LinkedList<ExtendsOf> extendsOf;
}
