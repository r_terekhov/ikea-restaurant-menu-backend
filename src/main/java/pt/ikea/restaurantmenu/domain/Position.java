package pt.ikea.restaurantmenu.domain;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = MenuPosition.class, name = "MENU_POSITION"),
        @JsonSubTypes.Type(value = OnlyPartOfPosition.class, name = "ONLY_PART_OF_POSITION"),
})
public interface Position {

    String getPositionType();
}
