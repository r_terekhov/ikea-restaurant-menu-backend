package pt.ikea.restaurantmenu.domain;


import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotBlank;

@NoArgsConstructor
public class Category extends Document {

    @Getter
    @NotBlank String name;

    @Getter
    @Nullable
    String description;

    @Getter
    @NotBlank
    String urlPhoto;

    @Getter
    int order;


    public Category(@NotBlank String name, @Nullable String description, @NotBlank String urlPhoto, int order) {
        this.name = name;
        this.description = description;
        this.urlPhoto = urlPhoto;
        this.order = order;
    }
}
