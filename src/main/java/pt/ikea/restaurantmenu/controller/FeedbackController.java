package pt.ikea.restaurantmenu.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pt.ikea.restaurantmenu.model.BaseResponse;
import pt.ikea.restaurantmenu.service.FeedbackService;

import javax.validation.constraints.NotBlank;

@RestController
@Validated
@RequestMapping("feedback")
public class FeedbackController {
    private static final Logger log = LoggerFactory.getLogger(FeedbackController.class);
    private final FeedbackService feedbackService;

    public FeedbackController(FeedbackService feedbackService) {
        this.feedbackService = feedbackService;
    }

    @PostMapping("create")
    public ResponseEntity<BaseResponse<String>> postFeedback(@RequestParam @NotBlank String feedback) {
        return new ResponseEntity<>(BaseResponse.response(feedbackService.postFeedback(feedback)), HttpStatus.CREATED);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody
    ResponseEntity<BaseResponse> onInvalidRequest(Exception ex) {
        log.error(ex.getMessage());
        BaseResponse response = BaseResponse.error(ex.getLocalizedMessage());
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
