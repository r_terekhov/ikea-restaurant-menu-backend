package pt.ikea.restaurantmenu.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pt.ikea.restaurantmenu.domain.Position;
import pt.ikea.restaurantmenu.domain.SpecialOffer;
import pt.ikea.restaurantmenu.model.BaseResponse;
import pt.ikea.restaurantmenu.service.MenuPositionService;

import java.util.List;

@RestController
@Validated
@RequestMapping("product")
public class MenuPositionController {
    private static final Logger log = LoggerFactory.getLogger(MenuPositionController.class);
    private final MenuPositionService menuPositionService;

    public MenuPositionController(MenuPositionService menuPositionService) {
        this.menuPositionService = menuPositionService;
    }

    @GetMapping("getAll")
    public ResponseEntity<BaseResponse<List<Position>>> getAllProducts() {
        log.info("request for getAllProducts");
        return new ResponseEntity<>(BaseResponse.response(menuPositionService.getAllPositions()), HttpStatus.OK);
    }

    @GetMapping("getAllSpecialOffers")
    public ResponseEntity<BaseResponse<List<SpecialOffer>>> getAllSpecialOffers() {
        return new ResponseEntity<>(BaseResponse.response(menuPositionService.getAllSpecialOffers()), HttpStatus.OK);
    }


    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody
    ResponseEntity<BaseResponse> onInvalidRequest(Exception ex) {
        log.error(ex.getMessage());
        BaseResponse response = BaseResponse.error(ex.getLocalizedMessage());
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
